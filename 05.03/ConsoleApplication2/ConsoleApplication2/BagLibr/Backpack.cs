﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LuxuryBags.BagLibr
{
    class Backpack : LuxuryBags.BagLibr.BagInfo
    {
        private double _backpackLength = 0;
        private int _backpackPockets = 0;
        public Backpack(int pocket)
        {
            this.BackpackPockets = pocket;
        }
        public string BagModel
        {
            get
            {
                return "Backpack";
            }
        }
        public double BackpackLength
        {
            get
            {
                return _backpackLength;
            }
            set
            {
                if (value != null)
                    _backpackLength = value;
            }
        }
        public int BackpackPockets
        {
            get
            {
                return _backpackPockets;
            }
            set
            {
                if (value != null)
                    _backpackPockets = value;
            }
        }
    }
}
