﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LuxuryBags.BagLibr
{
    class TravelBag : LuxuryBags.BagLibr.BagInfo
    {
        private double _travelbagSize = 0;
        private int _travelbagWheel = 0;
        public string BagModel
        {
            get
            {
                return "Travel Bag";
            }
        }
        public double TravelbagSize
        {
            get
            {
                return _travelbagSize;
            }
            set
            {
                if (value != null)
                    _travelbagSize = value;
            }
        }
        public int TravelbagWheel
        {
            get
            {
                return _travelbagWheel;
            }
            set
            {
                if (value != null)
                    _travelbagWheel = value;
            }
        }
        public double BagPrice
        {
            get
            {
                return base.BagPrice;
            }
            set
            {
                if (value >= 2000)
                {
                    base.BagPrice = value;
                }
            }
        }
    }
}
