﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LuxuryBags.BagLibr
{
    class Clutch : LuxuryBags.BagLibr.BagInfo
    {
        private bool _clutchStrap = false;
        private string _clutchForm = "";
        public string BagModel
        {
            get
            {
                return "Clutch";
            }
        }
        public bool ClutchStrap
        {
            get
            {
                return _clutchStrap;
            }
            set
            {
                if (value != null)
                    _clutchStrap = value;
            }
        }
        public string ClutchForm
        {
            get
            {
                return _clutchForm;
            }
            set
            {
                if (value != null)
                    _clutchForm = value;
            }
        }
    }
}
